﻿namespace AdrenalinePlugin.Model {
	public class Unit {
		public uint objectId;
		public string name;
		public bool isPlayer;
		public uint worldId;
		public string world;
		public uint jobId;
		public uint level;
		public uint ownerId;
		public uint npcNameId;
		public uint npcBaseId;
		public uint hp;
		public uint mana;
		public uint hpMax;
	}
}