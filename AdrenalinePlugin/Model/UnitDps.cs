﻿using System;
using Dalamud.Game.ClientState.Objects.Types;

namespace AdrenalinePlugin.Model {
	[Serializable]
	public class UnitDps {
		public uint objectId;
		public uint? ownerId;
		public uint? jobId;
		public string name = "???";
		public string companyTag = "";
		public float perSecond;
		public float total;

		public UnitDps(uint unitId, BattleChara? unit) {
			objectId = unitId;

			if (unit != null) {
				name = unit.Name.TextValue;
				ownerId = unit.OwnerId;
				jobId = unit.ClassJob.Id;
				companyTag = unit.CompanyTag.TextValue;
			}
		}

		public float CalculatePerSecond(double totalTime) {
			perSecond = total / (float)totalTime;
			return perSecond;
		}
	}
}