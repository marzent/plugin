﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Dalamud.Game.Text.SeStringHandling;
using Dalamud.Logging;
using Dalamud.Utility;
using FFXIV_ACT_Plugin.Standalone;

namespace AdrenalinePlugin.Service {
	public class ParseProcessor : IParseProcessor {
		public event Action<DateTime, uint, uint, long, uint, bool>? Damage;
		public event Action<DateTime, uint, uint, long, bool>? Heal;

		protected readonly ParseManager manager;

		public ParseProcessor() {
			manager = new ParseManager(this);
			manager.ProcessException += ProcessException;
		}

		protected void ProcessException(DateTime timestamp, string err) {
			PluginLog.LogError($"ERR: {timestamp}: {err}");
		}

		public void ProcessDamage(
			DateTime timestamp,
			uint sourceId,
			uint targetId,
			long amount,
			uint comboAmount,
			bool isCritical
		) {
			string line = $"{timestamp:O} {sourceId:X8} {targetId:X8} Damage {amount} {(isCritical ? "Critical" : "")}";
			PluginLog.Warning(line);
			Damage?.Invoke(timestamp, sourceId, targetId, amount, comboAmount, isCritical);
		}

		public void ProcessHeal(
			DateTime timestamp, 
			uint sourceId, 
			uint targetId, 
			long amount, 
			bool isCritical
		) {
			string line = $"{timestamp:O} {sourceId:X8} {targetId:X8} Heal {amount} {(isCritical ? "Critical" : "")}";
			PluginLog.Warning(line);
			Heal?.Invoke(timestamp, sourceId, targetId, amount, isCritical);
		}

		public void PushLogLine(byte logType, IEnumerable<object?> logLine) {
			var joined = logType.ToString("D2") + "|" + string.Join('|', logLine.Select(
				l => {
					return l switch {
						null => "",
						float f => f.ToString("F2", CultureInfo.InvariantCulture),
						double d => d.ToString("F2", CultureInfo.InvariantCulture),
						SeString ss => ss.TextValue,
						Lumina.Text.SeString lss => lss.ToDalamudString().TextValue,
						_ => l.ToString()
					};
				})) + "|0";

			PluginLog.Log($"LOG LINE: {joined}");
			// TODO: enable it
			manager.ProcessLine(DateTime.Now, joined);
			// TODO: implement it
		}
	}
}