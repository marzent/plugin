﻿using Dalamud.Game.ClientState.Objects.Types;
using Dalamud.Game.Network;
using Dalamud.Logging;
using Dalamud.Plugin.Ipc;
using Newtonsoft.Json;
using AdrenalinePlugin.Service.Network;

namespace AdrenalinePlugin.Service {
	public static class NetworkListener {
		internal static ICallGateSubscriber<ushort, string, NetworkMessageDirection, (object, uint?), bool>
			networkSub = null!;

		public static void Initialize() {
			networkSub = AdrenalinePlugin.pluginInterface.GetIpcSubscriber<
				ushort, string, NetworkMessageDirection, (object, uint?), bool
			>("NextUI.NetworkEvent");

			networkSub.Subscribe(NetworkEvent);
			AdrenalinePlugin.SubscribeIpcEvent("NetworkEvent");
		}

		public static void NetworkEvent(
			ushort opcode,
			string eventName,
			NetworkMessageDirection dir,
			(object, uint?) packet
		) {
			var (data, targetActorId) = packet;
			var chara = targetActorId == null ? null : (BattleChara?)AdrenalinePlugin.objectTable.SearchById(targetActorId.Value);
			if (chara != null) {
				CombatantManager.CheckCombatant(chara);
			}

			switch (eventName) {
				case "actorCast":
					ActorCast.StartCast(data, chara);
					break;
				case "actorControl":
					ActorControl.ActorControlGeneric(data, chara);
					break;
				case "actorControlSelf":
					ActorControl.ActorControlSelf(data, chara);
					break;
				case "actorControlTarget":
					ActorControl.ActorControlTarget(data, chara);
					break;
				case "effectResult":
					EffectResult.EffectResultFull(data, chara);
					break;
				case "effectResultBasic":
					EffectResult.EffectResultBasic(data, chara);
					break;
				case "actionEffect1":
					ActionEffect.ActionEffectAny(data, chara, 1);
					break;
				case "actionEffect8":
					ActionEffect.ActionEffectAny(data, chara, 8);
					break;
				case "actionEffect16":
					ActionEffect.ActionEffectAny(data, chara, 16);
					break;
				case "actionEffect24":
					ActionEffect.ActionEffectAny(data, chara, 24);
					break;
				case "actionEffect32":
					ActionEffect.ActionEffectAny(data, chara, 32);
					break;
				case "statusEffectList":
					StatusList.StatusEffectList(data, chara);
					break;
				case "statusEffectList2": // Same data format
					StatusList.StatusEffectList(data, chara);
					break;
				case "statusEffectList3":
					StatusList.StatusEffectList3(data, chara);
					break;
			}
		}

		public static void Shutdown() {
			networkSub.Unsubscribe(NetworkEvent);
		}
	}
}