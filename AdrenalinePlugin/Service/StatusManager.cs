﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dalamud.Game.ClientState.Objects.Types;
using Dalamud.Game.ClientState.Statuses;
using Dalamud.Logging;
using FFXIV_ACT_Plugin.Common;
using FFXIV_ACT_Plugin.Common.Models;

// using FFXIV_ACT_Plugin.Common;
// using FFXIV_ACT_Plugin.Common.Models;
// using FFXIV_ACT_Plugin.Logfile;
// using FFXIV_ACT_Plugin.Memory;
// using System;
//
// namespace FFXIV_ACT_Plugin.Network
// {
//     public interface INetworkBuffManager
//     {
//         void AddUpdateBuffForTarget(uint ActorId, uint TargetId, UInt16 BuffId, UInt16 BuffExtra, float Duration, DateTime packetDate, byte index, bool missingInfo);
//         void RemoveBuff(uint TargetId, int index, DateTime packetDate);
//
//         void ReplaceAllBuffsForTarget(uint TargetId, NetworkBuff[] buffs, DateTime packetDate);
//     }
//
//     public class NetworkBuffManager : INetworkBuffManager
//     {
//         private readonly ICombatantManager _combatantManager;
//         private readonly ILogOutput _logOutput;
//         private readonly Resource.IStatusList _buffList;
//         private readonly ILogFormat _logFormat;
//         private readonly DataEvent _dataEvent;
//
//         public NetworkBuffManager(ILogOutput logOutput, Resource.IStatusList buffList,
//             ICombatantManager combatantManager, ILogFormat logFormat, DataEvent dataEvent)
//         {
//             _logOutput = logOutput;
//             _logFormat = logFormat;
//             _dataEvent = dataEvent;
//             _buffList = buffList;
//             _combatantManager = combatantManager;
//         }
//
//         public void AddUpdateBuffForTarget(uint ActorId, uint TargetId, ushort BuffId, ushort BuffExtra, float Duration, DateTime packetDate, byte index, bool missingInfo)
//         {
//             Combatant Target = _combatantManager.GetCombatantById(TargetId);
//             if (Target == null)
//                 return;
//
//             NetworkBuff networkBuff = Target.NetworkBuffs[index];
//             if (networkBuff == null)
//                 networkBuff = Target.NetworkBuffs[index] = new NetworkBuff();
//
//             bool changed = false;
//
//             // fix Duration so it doesnt look like buff is being lost.
//             if (Duration == 0)
//                 Duration = 9999;
//
//             if (networkBuff.BuffID == 0)
//             {
//                 // new buff - add
//                 changed = true;
//             }
//             else if (BuffId == networkBuff.BuffID && ActorId == networkBuff.ActorID)
//             {
//                 // check duration for change
//                 if (Duration != 9999)
//                     if (Math.Abs((packetDate.AddSeconds(Duration) - networkBuff.Timestamp.AddSeconds(networkBuff.Duration)).TotalSeconds) > 2)
//                         changed = true;
//
//                 // check stacks for change
//                 if (BuffExtra != networkBuff.BuffExtra)
//                     changed = true;
//             }
//             else if (missingInfo)
//             {
//                 // missing both Actor Id and Duration, so only check for buff stack changes.
//
//                 // check stacks for change
//                 if (BuffExtra != networkBuff.BuffExtra)
//                 {
//                     changed = true;
//                     ActorId = networkBuff.ActorID;
//                 }
//             }
//             else
//             {
//                 // todo: active buff, but buff at that index doesnt match!
//                 RemoveBuff(TargetId, index, packetDate);
//                 changed = true;
//             }
//
//             if (changed)
//             {
//                 Combatant Actor = _combatantManager.GetCombatantById(ActorId);
//
//                 if (!missingInfo)
//                 {
//                     networkBuff.ActorID = ActorId;
//                     networkBuff.ActorName = Actor?.Name;
//                     networkBuff.Duration = Duration;
//                     networkBuff.Timestamp = packetDate;
//                 }
//                 networkBuff.BuffExtra = BuffExtra;
//                 networkBuff.BuffID = BuffId;
//                 networkBuff.TargetID = TargetId;
//                 networkBuff.TargetName = Target.Name;
//
//                 string message = _logFormat.FormatNetworkBuffMessage(
//                     networkBuff.BuffID, _buffList.GetStatusNameByID(networkBuff.BuffID), networkBuff.Duration,
//                     networkBuff.ActorID, networkBuff.ActorName, networkBuff.TargetID, networkBuff.TargetName,
//                     networkBuff.BuffExtra, Target.MaxHP, Actor?.MaxHP);
//
//
//                 _logOutput.WriteLine(LogMessageType.StatusAdd, packetDate, message);
//                 _dataEvent.OnParsedLogLine(LogMessageType.StatusAdd, message);
//             }
//         }
//
//         public void RemoveBuff(uint TargetId, int index, DateTime packetDate)
//         {
//             Combatant Target = _combatantManager.GetCombatantById(TargetId);
//             if (Target == null)
//                 return;
//             NetworkBuff buff = Target.NetworkBuffs[index];
//             if (buff == null)
//                 return;
//
//             Combatant Actor = _combatantManager.GetCombatantById(buff.ActorID);
//
//             string message = _logFormat.FormatNetworkBuffMessage(
//                 buff.BuffID, _buffList.GetStatusNameByID(buff.BuffID), 0,
//                 buff.ActorID, buff.ActorName, buff.TargetID, buff.TargetName,
//                 buff.BuffExtra, Target?.MaxHP, Actor?.MaxHP);
//
//             ClearBuff(buff);
//
//             _logOutput.WriteLine(LogMessageType.StatusRemove, packetDate, message);
//             _dataEvent.OnParsedLogLine(LogMessageType.StatusRemove, message);
//         }
//
//         public void ReplaceAllBuffsForTarget(UInt32 TargetId, NetworkBuff[] buffs, DateTime packetDate)
//         {
//             Combatant Target = _combatantManager.GetCombatantById(TargetId);
//             if (Target == null)
//                 return;
//
//             if (buffs.Length > Target.NetworkBuffs.Length)
//                 Array.Resize(ref Target.NetworkBuffs, buffs.Length);
//
//             // Do not try to update buffs at all if target isnt loaded.
//             for (byte i = 0; i < Target.NetworkBuffs.Length; i++)
//             {
//                 // Note - if boss has both 30- and 60-buff packets 
//                 if (i >= buffs.Length)
//                 {
//                     // if buff is populated, clear it.
//                     if (Target.NetworkBuffs[i]?.BuffID > 0)
//                         RemoveBuff(TargetId, i, packetDate);
//                     continue;
//                 }
//
//                 // ignore certain buffs
//                 if (_buffList.IsIgnoreStatus(buffs[i].BuffID))
//                     continue;
//
//                 if (Target.NetworkBuffs[i] == null)
//                     Target.NetworkBuffs[i] = new NetworkBuff();
//
//                 // Add, Update, or Remove.
//                 if (buffs[i].BuffID != 0)
//                     AddUpdateBuffForTarget(buffs[i].ActorID, TargetId, buffs[i].BuffID, buffs[i].BuffExtra, buffs[i].Duration, packetDate, i, false);
//                 else if (Target.NetworkBuffs[i].BuffID != 0)
//                     RemoveBuff(TargetId, i, packetDate);
//             }
//         }
//
//         private void ClearBuff(NetworkBuff buff)
//         {
//             buff.ActorID = 0;
//             buff.ActorName = "";
//             buff.BuffExtra = 0;
//             buff.BuffID = 0;
//             buff.Duration = 0;
//             buff.TargetID = 0;
//             buff.TargetName = "";
//             buff.Timestamp = DateTime.MinValue;
//         }
//
//         //private string DebugDumpBuffs(List<NetworkBuff> Buffs)
//         //{
//         //    string ret = "";
//         //    foreach (NetworkBuff buff in Buffs)
//         //        ret += buff.BuffID.ToString("X2") + "|" + _buffList.GetBuffNameByID(buff.BuffID) + "|" + buff.ActorID.ToString("X8") + "|" + buff.BuffExtra.ToString("X8") + "   ";
//
//         //    return ret;
//         //}
//     }
// }

namespace AdrenalinePlugin.Service {
	public class NuStatus {
		public uint statusId;
		public uint sourceId;
		public ushort stackCount;
		public float duration;
		public int index;
	}

	public static class StatusManager {
		internal static Dictionary<uint, List<NuStatus>> statuses = new();

		public static NuStatus? GetStatusByIndex(BattleChara chara, int index) {
			if (!statuses.TryGetValue(chara.ObjectId, out var statusList)) {
				statusList = new List<NuStatus>();
				statuses.Add(chara.ObjectId, statusList);
			}

			var stat = statusList.FirstOrDefault(s => s.index == index);
			if (stat == null || stat.statusId == 0) {
				return null;
			}

			return stat;
		}

		public static bool AddStatus(BattleChara chara, NuStatus status) {
			if (!statuses.TryGetValue(chara.ObjectId, out var statusList)) {
				statusList = new List<NuStatus>();
				statuses.Add(chara.ObjectId, statusList);
			}

			statusList.Add(status);
			return true;
		}

		public static NuStatus? RemoveStatusByIndex(BattleChara chara, int index) {
			if (!statuses.TryGetValue(chara.ObjectId, out var statusList)) {
				return null;
			}

			var stat = statusList.FirstOrDefault(s => s.index == index);
			if (stat == null) {
				return null;
			}

			statusList.RemoveAll(s => s.index == index);

			if (statusList.Count == 0) {
				statuses.Remove(chara.ObjectId);
			}

			return stat;
		}

		public static void AddUpdateBuffForTarget(
			uint sourceId,
			BattleChara? target,
			uint statusId,
			ushort stackCount,
			float duration,
			int index,
			bool missingInfo
		) {
			if (target == null) {
				return;
			}

			// fix Duration so it doesnt look like buff is being lost.
			if (duration == 0) {
				duration = 9999;
			}

			var changed = false;

			var currentStatus = GetStatusByIndex(target, index);
			// TODO: think about this
			if (currentStatus == null) {
				// new buff - add
				changed = true;
			}

			// NetworkBuff networkBuff = Target.NetworkBuffs[index];
			// if (networkBuff == null)
			// 	networkBuff = Target.NetworkBuffs[index] = new NetworkBuff();

			if (currentStatus != null) {
				if (statusId == currentStatus.statusId && sourceId == currentStatus.sourceId) {
					// check duration for change
					if (duration >= 9999 && Math.Abs(duration - currentStatus.duration) > 2) {
						changed = true;
					}

					// check stacks for change
					if (stackCount != currentStatus.stackCount) {
						changed = true;
					}
				}
				else if (missingInfo) {
					// missing both Actor Id and Duration, so only check for buff stack changes.

					// check stacks for change
					if (stackCount != currentStatus.stackCount) {
						changed = true;
						sourceId = currentStatus.sourceId;
					}
				}
				else {
					// todo: active buff, but buff at that index doesnt match!
					RemoveBuff(target, index);
					changed = true;
				}
			}

			if (!changed) {
				return;
			}

			var source = (BattleChara?)AdrenalinePlugin.objectTable.SearchById(sourceId);
			// Combatant Actor = _combatantManager.GetCombatantById(ActorId);

			if (currentStatus == null) {
				currentStatus = new NuStatus();

				if (!missingInfo) {
					currentStatus.sourceId = sourceId;
					currentStatus.duration = duration;
				}

				currentStatus.stackCount = stackCount;
				currentStatus.statusId = statusId;
				currentStatus.index = index;

				AddStatus(target, currentStatus);
			}
			else {
				currentStatus.duration = duration;
				currentStatus.stackCount = stackCount;
			}

			StatusAddLogLine(AdrenalinePlugin.Timestamp(), statusId, duration, stackCount, source, target);
		}

		public static void RemoveBuff(
			BattleChara? target,
			int index
		) {
			if (target == null) {
				return;
			}

			var buff = RemoveStatusByIndex(target, index);
			if (buff == null) {
				return;
			}

			var source = (BattleChara?)AdrenalinePlugin.objectTable.SearchById(buff.sourceId);
			StatusRemoveLogLine(AdrenalinePlugin.Timestamp(), buff.statusId, buff.stackCount, source, target);
		}

		public static void ReplaceAllBuffsForTarget(
			BattleChara? target,
			IEnumerable<NuStatus> newStatuses
		) {
			// Do not try to update buffs at all if target isn't loaded.
			if (target == null) {
				return;
			}

			var existingIndexes = new List<int>();
			foreach (var newStatus in newStatuses) {
				existingIndexes.Add(newStatus.index);
				AddUpdateBuffForTarget(
					newStatus.sourceId,
					target,
					newStatus.statusId,
					newStatus.stackCount,
					newStatus.duration,
					newStatus.index,
					false
				);
			}

			if (!statuses.TryGetValue(target.ObjectId, out var statusList)) {
				return;
			}

			var statusesToRemove = statusList.ToArray().Where(s => !existingIndexes.Contains(s.index));
			foreach (var statusToRemove in statusesToRemove) {
				RemoveBuff(target, statusToRemove.index);
			}
		}

		public static void StatusAddLogLine(
			string timestamp,
			uint statusId,
			float duration,
			ushort count,
			BattleChara? source,
			BattleChara target
		) {
			var status = AdrenalinePlugin.statusSheet.GetRow(statusId);

			var networkBuffLogLine = new object?[] {
				timestamp, statusId.ToString("X"), status?.Name, duration,
				source?.ObjectId.ToString("X8") ?? "", source?.Name ?? "",
				target.ObjectId.ToString("X8"), target.Name,
				count, target.MaxHp, source?.MaxHp
			};
			AdrenalinePlugin.processor.PushLogLine(26, networkBuffLogLine);
		}

		public static void StatusRemoveLogLine(
			string timestamp,
			uint statusId,
			ushort count,
			BattleChara? source,
			BattleChara target
		) {
			var status = AdrenalinePlugin.statusSheet.GetRow(statusId);

			var networkBuffLogLine = new object?[] {
				timestamp, statusId.ToString("X"), status?.Name, 0,
				source?.ObjectId.ToString("X8") ?? "", source?.Name ?? "",
				target.ObjectId.ToString("X8"), target.Name,
				count, target.MaxHp, source?.MaxHp
			};

			AdrenalinePlugin.processor.PushLogLine(30, networkBuffLogLine);
		}
	}
}