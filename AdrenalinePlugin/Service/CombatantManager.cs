﻿using System.Collections.Generic;
using System.Linq;
using System.Timers;
using Dalamud.Game.ClientState.Objects.SubKinds;
using Dalamud.Game.ClientState.Objects.Types;
using Dalamud.Utility;
using AdrenalinePlugin.Model;

namespace AdrenalinePlugin.Service {
	public static class CombatantManager {
		internal static readonly List<Unit> tracked = new();
		internal static readonly Timer timer = new(10000);

		public static void Initialize() {
			timer.Elapsed += TimerOnElapsed;
			timer.Start();
		}

		public static void Shutdown() {
			timer.Elapsed -= TimerOnElapsed;
			timer.Stop();
		}

		internal static void TimerOnElapsed(object sender, ElapsedEventArgs e) {
			CheckAll();
		}

		public static void AddCombatant(BattleChara chara) {
			if (chara.ObjectId == AdrenalinePlugin.EnvId) {
				return;
			}

			var unit = tracked.Find(u => u.objectId == chara.ObjectId);
			if (unit != null) {
				return;
			}

			unit = new Unit {
				objectId = chara.ObjectId,
				name = chara.Name.TextValue,
				level = chara.Level,
				ownerId = chara.OwnerId,
				npcNameId = chara.NameId,
				npcBaseId = chara.NameId,
				mana = chara.CurrentMp,
				hp = chara.CurrentHp,
				hpMax = chara.MaxHp,
			};

			if (chara is PlayerCharacter playerCharacter) {
				unit.world = playerCharacter.HomeWorld?.GameData?.Name.ToDalamudString().TextValue ?? "";
				unit.worldId = playerCharacter.HomeWorld?.Id ?? 0;
				unit.isPlayer = true;
			}

			tracked.Add(unit);
			CombatantAddLogLine(unit);
		}

		public static void CheckAll() {
			var trackedIds = tracked.ToArray().Select(t => t.objectId);
			foreach (var trackedId in trackedIds) {
				CheckCombatant(trackedId);
			}

			foreach (var obj in AdrenalinePlugin.objectTable) {
				if (obj is not BattleChara chara) {
					continue;
				}

				CheckCombatant(chara);
			}
		}

		public static void CheckCombatant(BattleChara chara) {
			var unit = tracked.Find(u => u.objectId == chara.ObjectId);

			// exists in object table but not tracked yet
			if (unit == null) {
				AddCombatant(chara);
			}
		}

		public static void CheckCombatant(uint objectId) {
			var chara = (BattleChara?)AdrenalinePlugin.objectTable.SearchById(objectId);
			var unit = tracked.Find(u => u.objectId == objectId);

			if (chara == null) {
				// does not exist in object table but exists in tracking
				if (unit != null) {
					tracked.Remove(unit);
					CombatantRemoveLogLine(unit);
				}
			}
			else {
				// exists in object table but not tracked yet
				if (unit == null) {
					AddCombatant(chara);
				}
			}
		}

		public static void CombatantAddLogLine(Unit chara) {
			var networkBuffLogLine = new object?[] {
				AdrenalinePlugin.Timestamp(), chara.objectId.ToString("X8"), chara.name,
				chara.jobId, chara.level, chara.ownerId.ToString("X8"), chara.worldId, chara.world,
				chara.npcNameId, chara.npcBaseId,
				chara.hp, chara.hpMax,
				chara.mana, 10000U,
				null, null,
				0f, 0f, 0f, 0f,
			};

			AdrenalinePlugin.processor.PushLogLine(03, networkBuffLogLine);
		}

		public static void CombatantRemoveLogLine(Unit chara) {
			var networkBuffLogLine = new object?[] {
				AdrenalinePlugin.Timestamp(), chara.objectId.ToString("X8"), chara.name,
				chara.jobId, chara.level, chara.ownerId.ToString("X8"), chara.worldId, chara.world,
				chara.npcNameId, chara.npcBaseId,
				chara.hp, chara.hpMax,
				chara.mana, 10000U,
				null, null,
				0f, 0f, 0f, 0f,
			};

			AdrenalinePlugin.processor.PushLogLine(04, networkBuffLogLine);
		}
	}
}