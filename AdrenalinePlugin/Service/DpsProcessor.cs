﻿using System;
using System.Collections.Generic;
using System.Timers;
using Dalamud.Logging;
using AdrenalinePlugin.Model;
using Dalamud.Game.ClientState.Objects.Types;

namespace AdrenalinePlugin.Service {
	public class DpsProcessor : IDisposable {
		public List<UnitDps> dpsChart = new();
		protected Timer timer = new(1000);
		public double timeInCombat;
		public DateTime combatStarted;
		protected ParseProcessor processor;

		public event Action<List<UnitDps>>? Tick; 

		public DpsProcessor(ParseProcessor pr) {
			processor = pr;
			processor.Damage += ProcessorOnDamage;
			timer.Elapsed += TimerOnElapsed;
		}

		protected void TimerOnElapsed(object sender, ElapsedEventArgs e) {
			timeInCombat = (e.SignalTime - combatStarted).TotalSeconds;
			Calculate();
		}

		public void Calculate() {
			foreach (var dpsEntry in dpsChart) {
				dpsEntry.CalculatePerSecond(timeInCombat);
			}

			Tick?.Invoke(dpsChart);
		}
		
		protected void ProcessorOnDamage(
			DateTime timestamp,
			uint sourceId,
			uint targetId,
			long amount,
			uint comboAmount,
			bool isCritical
		) {
			var unit = dpsChart.Find(d => d.objectId == sourceId);
			if (unit == null) {
				var source = (BattleChara?)AdrenalinePlugin.objectTable.SearchById(sourceId);
				unit = new UnitDps(sourceId, source);
				dpsChart.Add(unit);
			}

			unit.total += amount;
		}

		public void CombatStateChanged(bool inCombat) {
			if (inCombat) {
				dpsChart = new List<UnitDps>();
				timeInCombat = 0f;
				combatStarted = DateTime.Now;
				timer.Start();
				return;
			}

			timer.Stop();
			Calculate();
		}

		public void Dispose() {
			processor.Damage -= ProcessorOnDamage;
			timer.Elapsed -= TimerOnElapsed;
			timer.Dispose();
			GC.SuppressFinalize(this);
		}
	}
}