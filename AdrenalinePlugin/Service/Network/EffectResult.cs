﻿using Dalamud.Game.ClientState.Objects.Types;

namespace AdrenalinePlugin.Service.Network {
	public static class EffectResult {
		public static void EffectResultBasic(dynamic packet, BattleChara? chara) {
			var timestamp = AdrenalinePlugin.Timestamp();
			var effectResultLogLine = new object?[] {
				timestamp, chara?.ObjectId.ToString("X8"), chara?.Name,
				packet.relatedActionSequence.ToString("X8"), 
				packet.currentHp, null, 
				null, null, 
				null, null,
				chara?.Position.X, chara?.Position.Z, chara?.Position.Y, chara?.Rotation, //numArray
			};

			AdrenalinePlugin.processor.PushLogLine(37, effectResultLogLine);
		}

		public static void EffectResultFull(dynamic packet, BattleChara? chara) {
			byte effectCount = packet.effectCount;
			if (effectCount > 4) {
				effectCount = 4;
			}

			var timestamp = AdrenalinePlugin.Timestamp();
			var effectResultLogLine = new object?[] {
				timestamp, chara?.ObjectId.ToString("X8"), chara?.Name,
				packet.relatedActionSequence.ToString("X8"), 
				packet.currentHp, packet.maxHp, 
				packet.currentMp, 10000U,
				packet.damageShield, null,
				chara?.Position.X, chara?.Position.Z, chara?.Position.Y, chara?.Rotation
				//numArray
			};
			AdrenalinePlugin.processor.PushLogLine(37, effectResultLogLine);

			for (var i = 0; i < effectCount; i++) {
				var statusEntry = packet.statusEntries[i];
				StatusManager.AddUpdateBuffForTarget(
					(uint)statusEntry.sourceActorId,
					chara,
					(uint)statusEntry.id,
					(ushort)statusEntry.unknown2,
					(float)statusEntry.duration,
					(byte)statusEntry.index,
					false
				);
			}
		}
	}
}