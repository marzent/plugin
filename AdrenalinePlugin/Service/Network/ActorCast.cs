﻿using Dalamud.Game.ClientState.Objects.Types;
using Dalamud.Logging;

namespace AdrenalinePlugin.Service.Network {
	public static class ActorCast {
		public static void StartCast(dynamic packet, BattleChara? chara) {
			if (chara == null) {
				// figure this out
				PluginLog.Warning("NetworkStartsCasting chara is null");
				return;
			}

			var action = AdrenalinePlugin.actionSheet.GetRow((uint)packet.actionId);
			var target = AdrenalinePlugin.objectTable.SearchById((uint)packet.targetId);

			var ll = new object?[] {
				AdrenalinePlugin.Timestamp(), chara.ObjectId.ToString("X8"), chara.Name,
				packet.actionId.ToString("X"), action?.Name,
				target?.ObjectId.ToString("X8"), target?.Name,
				packet.castTime,
				chara.Position.X, chara.Position.Z, chara.Position.Y, chara.Rotation
			};
			AdrenalinePlugin.processor.PushLogLine(20, ll);
		}
	}
}