﻿using System.Collections.Generic;
using Dalamud.Game.ClientState.Objects.Types;

namespace AdrenalinePlugin.Service.Network {
	public static class ActionEffect {
		public static void ActionEffectAny(
			dynamic packet,
			BattleChara? chara,
			byte maxTargetCount
		) {
			// TODO: WTF?
			var effectDisplayType = (int)packet.header.effectDisplayType;
			uint num1 = effectDisplayType != 13
				? (effectDisplayType != 2
					? (uint)packet.header.actionAnimationId
					: 33554432U + packet.header.actionId)
				: 218103808U + packet.header.actionId;

			var action = AdrenalinePlugin.actionSheet.GetRow(num1);

			byte targetCount = packet.header.effectCount;
			if (targetCount > maxTargetCount) {
				targetCount = maxTargetCount;
			}

			var messageType = maxTargetCount <= 1 ? (byte)21 : (byte)22;

			if (targetCount == 0) {
				var logLine = new List<object?> {
					AdrenalinePlugin.Timestamp(), chara?.ObjectId.ToString("X8"), chara?.Name,
					num1.ToString("X"), action?.Name,
					AdrenalinePlugin.EnvId.ToString("X8"), ""
				};

				for (var i = 0; i < 16; i++) {
					logLine.Add(packet.effects[i].ToString("X"));
				}

				logLine.AddRange(new object?[] {
					0, 0,
					0, 0,
					null, null,
					0, 0, 0, 0,

					chara?.CurrentHp, chara?.MaxHp,
					chara?.CurrentMp, chara?.MaxMp,
					null, null,
					chara?.Position.X, chara?.Position.Z, chara?.Position.Y, chara?.Rotation,

					packet.header.globalEffectCounter.ToString("X8"), 0, 0
				});

				AdrenalinePlugin.processor.PushLogLine(messageType, logLine);
			}
			else {
				for (byte index = 0; index < targetCount; ++index) {
					var targetId = (uint)packet.targetIds[index];
					if (targetId == 0U) {
						continue;
					}

					var target = (BattleChara?)AdrenalinePlugin.objectTable.SearchById(targetId);

					var logLine = new List<object?> {
						AdrenalinePlugin.Timestamp(), chara?.ObjectId.ToString("X8"), chara?.Name,
						num1.ToString("X"), action?.Name,
						targetId.ToString("X8"), target?.Name,
					};

					for (var i = 0; i < 16; i++) {
						logLine.Add(packet.effects[i].ToString("X"));
					}

					logLine.AddRange(new object?[] {
						target?.CurrentHp, target?.MaxHp,
						target?.CurrentMp, 10000U,
						null, null,
						target?.Position.X, target?.Position.Z, target?.Position.Y, target?.Rotation,

						chara?.CurrentHp, chara?.MaxHp,
						chara?.CurrentMp, chara?.MaxMp,
						null, null,
						chara?.Position.X, chara?.Position.Z, chara?.Position.Y, chara?.Rotation,

						packet.header.globalEffectCounter.ToString("X8"), index, targetCount
					});

					AdrenalinePlugin.processor.PushLogLine(messageType, logLine);
				}
			}
		}
	}
}